<?php

namespace Drupal\mongodb\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\Date as CoreDate;
use MongoDB\BSON\UTCDateTime;

/**
 * Overriding the views filter plugin "date".
 */
class Date extends CoreDate {

  use DateTrait; 

  /**
   * {@inheritdoc}
   */
  protected function opBetween($field) {
    $a = intval(strtotime($this->value['min'], 0));
    $b = intval(strtotime($this->value['max'], 0));

    if (!empty($this->value['type']) && $this->value['type'] == 'offset') {
      $a = new UTCDateTime((REQUEST_TIME - $a) * 1000);
      $b = new UTCDateTime((REQUEST_TIME + $b) * 1000);
    }
    else {
      $a = new UTCDateTime($a * 1000);
      $b = new UTCDateTime($b * 1000);
    }
    if (strtoupper($this->operator) == 'BETWEEN') {
      $this->query->addCondition($this->options['group'], $field, $a, '>=');
      $this->query->addCondition($this->options['group'], $field, $b, '<=');
    }
    else {
      $condition = $this->handler->query->getConnection()->condition('OR');
      $condition->condition($field, $a, '<');
      $condition->condition($field, $b, '>');
      $this->query->addCondition($this->options['group'], $condition);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function opSimple($field) {
    $value = intval(strtotime($this->value['value'], 0));

    if (!empty($this->value['type']) && $this->value['type'] == 'offset') {
      $value = new UTCDateTime((REQUEST_TIME + $value) * 1000);
    }
    else {
      $value = new UTCDateTime($value * 1000);
    }
    $this->query->addCondition($this->options['group'], $field, $value, strtoupper($this->operator));
  }

}
