<?php

namespace Drupal\mongodb\Driver\Database\mongodb;

use Drupal\Core\Database\Transaction as DatabaseTransaction;

/**
 * The MongoDB implementation of \Drupal\Core\Database\Transaction.
 */
class Transaction extends DatabaseTransaction { }
