<?php

namespace Drupal\mongodb\Driver\Database\mongodb;

use Drupal\Core\Database\StatementInterface;

/**
 * The MongoDB implementation of the StatementInterface for count queries.
 */
class StatementCountQuery implements \Iterator, StatementInterface {

  /**
   * The result of the count query.
   *
   * @var int
   */
  protected $count;

  /**
   * The query in a string format.
   *
   * @var string
   */
  protected $queryString;

  /**
   * Reference to the Drupal database connection object for this statement.
   *
   * @var \Drupal\mongodb\Driver\Connection
   */
  public $connection;

  /**
   * The constructor.
   *
   * @param \Drupal\mongodb\Driver\Connection $connection
   *   Database connection object for MongoDB.
   * @param int $count
   *   The result of the count query.
   * @param array $options
   *   An array of options for the count query.
   */
  public function __construct(Connection $connection, $count, array $options) {
    // The specific variable name is needed by the database query logger.
    $this->connection = $connection;

    // SQL queries return string values.
    $this->count = (string) $count;

    // This is only needed by the database query logger.
    $this->queryString = isset($options['query_string']) ? $options['query_string'] : '';

    // Change the database connection for the database query logger.
    if (isset($options['target']) && $options['target'] != $this->connection->getTarget()) {
      $this->connection = Database::getConnection($options['target']);
    }

    $logger = $this->connection->getLogger();
    if (!empty($logger)) {
      $logger->log($this, '', $options['query_time'], $options['query_start']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function execute($args = [], $options = []) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryString() {
    return $this->queryString;
  }

  /**
   * {@inheritdoc}
   */
  public function rowCount() {
    return $this->count;
  }

  /**
   * {@inheritdoc}
   */
  public function setFetchMode($mode, $a1 = NULL, $a2 = []) {
    return;
  }

  /**
   * {@inheritdoc}
   */
  public function fetch($mode = NULL, $cursor_orientation = NULL, $cursor_offset = NULL) {
    return $this->count;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchField($index = 0) {
    return $this->count;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchObject() {
    return $this->count;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchAssoc() {
    return $this->count;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchAll($mode = NULL, $column_index = NULL, $constructor_arguments = NULL) {
    return [$this->count];
  }

  /**
   * {@inheritdoc}
   */
  public function fetchCol($index = 0) {
    return [$this->count];
  }

  /**
   * {@inheritdoc}
   */
  public function fetchAllKeyed($key_index = 0, $value_index = 1) {
    return [$this->count];
  }

  /**
   * {@inheritdoc}
   */
  public function fetchAllAssoc($key, $fetch = NULL) {
    return [$this->count];
  }

  /**
   * {@inheritdoc}
   */
  public function current() {
    return $this->count;
  }

  /**
   * {@inheritdoc}
   */
  public function key() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function rewind() {
    // Nothing to do: our DatabaseStatement can't be rewound.
  }

  /**
   * {@inheritdoc}
   */
  public function next() {
    // Do nothing, since this is an always-empty implementation.
  }

  /**
   * {@inheritdoc}
   */
  public function valid() {
    return TRUE;
  }

}
