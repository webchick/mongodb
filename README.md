# Drupal on MongoDB

This project is still in full development and is not ready to be used in production!

This project is designed to work together with the Drumongous project (https://gitlab.com/daffie/drumongous).
The Drumongous project is the Drupal core project with a patch file that makes the necessary changes to make it all work.

# Requirements

 - The MongoDB database version 3.6 or higher.
 - The PHP-extension for MongoDB version 1.5 or higher.

# Installation

If you want to try out this software then run the following commands:

```
composer create-project daffie/drumongous:8.x-dev my_site_name_dir
cd my_site_dir
composer install
```

# Key points to remember when working with Drupal on MongoDB:

 - This project needs the PHP-extension for MongoDB version 1.5.
 - The driver stores all content entity data per entity instance in a single document.
   All entity data includes all revisional data, all translatable data and all field data.
   All entity data is stored per entity instance in a single document in a collection.
   The single document in a collection in MongoDB is in a single row in a table for relational databases.
 - The Drupal Database API is supported and the PDO statement interface is implemented.
 - The Drupal Entity API is supported.
 - The Drupal Entity Search API is supported, with the exception of the relationship part.
 - The Drupal Views module is supported. Some things work a little bit different, because the underlying table structure is different.
   Also some date conversion options are not supported by MongoDB.
 - MongoDB does not support SQL. It has its own query language. In SQL everything is written as a string.
   You can mix the char "4" and the integer 4 without any problems. With MongoDB you cannot do this.
   Also all methods in Drupal's database abstraction layer that takes SQL strings as input are not supported.
   (like db_query() and db_select->where())
 - This project stores data in the by MongoDB prefered BSON-types (See: https://docs.mongodb.com/manual/reference/bson-types/).
   This is different from the Drupal project choices:

| Type | Drupal | MongoDB |
| --- | --- | --- |
| Boolean | Integer | Boolean |
| Date | Integer | UTCDateTime object (in milliseconds) |
| Numeric | Decimal | Decimal128 object |
